﻿using System;
using System.Diagnostics;
using System.Text;

namespace dz_reflection
{
    class Program
    {
        public static int count { get; set; } = 100_000;

        static void Main(string[] args)
        {
            DoReflectionTest();
            DoJsonTest();
        }

        public static void DoReflectionTest()
        {
            TestSerializerDeserializer tsd = new TestSerializerDeserializer();
            string testObjStr = String.Empty;
            Stopwatch sw ;//= new Stopwatch();

            Console.WriteLine($"Reflection: Сериализация. Количество итераций: {count}");
            sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                testObjStr = tsd.DoSerializeReflection();
            }
            sw.Stop();

            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
            Console.WriteLine($"Строка с значениями:\n {testObjStr}");

            Console.WriteLine("Reflection: Десериализация");
            sw = Stopwatch.StartNew();
            for (int j = 0; j < count; j++)
            {
                tsd.DoDeserializeReflection(testObjStr);
            }
            sw.Stop();

            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
        }

        public static void DoJsonTest()
        {
            TestSerializerDeserializer tsd2 = new TestSerializerDeserializer();
            string testObjStrJson = string.Empty;
            Stopwatch sw;
            
            Console.WriteLine($"NewtonsoftJson: Сериализация. Количество итераций: {count}");
            sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                testObjStrJson = tsd2.DoSerializeJSON();
            }
            sw.Stop();
            
            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
            Console.WriteLine($"Строка с значениями:\n {testObjStrJson}");

            Console.WriteLine("NewtonsoftJson: Десериализация");
            sw = Stopwatch.StartNew();
            for (int j = 0; j < count; j++)
            {
                tsd2.DoDeserializeJSON(testObjStrJson);
            }
            sw.Stop();
            
            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
        }
    }
}