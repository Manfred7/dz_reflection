﻿﻿using System;
using System.Reflection;
using Newtonsoft.Json;

namespace dz_reflection
{
    public class TestSerializerDeserializer
    {
        private TestClass to;
        public void  ChangeFieldValues()
        {
            to.i1 = 10;
            to.i2 = 20;
            to.i3 = 30;
            to.i4 = 40;
            to.i5 = 50;
        }
        public TestSerializerDeserializer()
        {
            to = new TestClass() {i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5};
        }

        public string DoSerializeReflection()
        {
            Type myType = to.GetType();
            FieldInfo[] fields = myType.GetFields();
            string res = string.Empty;
            
            foreach (var field in fields)
            {
                res = res + $"{field.Name}={field.GetValue(to)};";
            }
            return res;
        }

        public void PrintFields()
        {
            Type myType = to.GetType();
            FieldInfo[] fields = myType.GetFields();
            
            foreach (var field in fields)
            {
                Console.WriteLine($"name: {field.Name} value:{field.GetValue(to)}");
            }
        }
        public void DoDeserializeReflection(string source)
        {
            Type myType = to.GetType();
            FieldInfo field;
            
            string[] sfields = source.Split(';');
            string[] sfield;
            sfield = source.Split(':');
            foreach (var f in sfields)
            {
                sfield = f.Split('=');
                if (sfield[0] != "")
                {
                    field = myType.GetField(sfield[0]);
                    field.SetValue(to,int.Parse(sfield[1]));
                }
            }
        }

        public string DoSerializeJSON()
        { 
            return  JsonConvert.SerializeObject(to);
        }
        
        public void DoDeserializeJSON(string source)
        { 
            to = JsonConvert.DeserializeObject<TestClass>(source);
        }
        
    }
}